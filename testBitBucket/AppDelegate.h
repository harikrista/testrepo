//
//  AppDelegate.h
//  testBitBucket
//
//  Created by Hari K Bista on 11/17/14.
//  Copyright (c) 2014 geekylemon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

