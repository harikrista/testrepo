//
//  main.m
//  testBitBucket
//
//  Created by Hari K Bista on 11/17/14.
//  Copyright (c) 2014 geekylemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
